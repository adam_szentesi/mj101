using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : Tileable
{
  public Color Color;
  public float LightLevel = 1.0f;
  public bool IsActive = true;

  public void Init(Vector2Int tilePosition, bool isActive)
  {
    base.Init(tilePosition);

    name = "Tile[" + tilePosition.x + ";" + tilePosition.y + "]";
    IsActive = isActive;
  }

  public void SetColor(Color color)
  {
    Color = color;
    UpdateColor();
  }

  public void SetLightLevel(float lightLevel)
  {
    LightLevel = lightLevel;
    UpdateColor();
  }

  private void UpdateColor()
  {
    Color color = Color * LightLevel;
    color.a = 1.0f;
    Renderer.color = color;
  }

  public void CopyTo(Tile tile)
  {
    tile.SetColor(Color);
  }

}
