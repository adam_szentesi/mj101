using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tileable : MonoBehaviour
{
  public Vector2Int TilePosition;
  public SpriteRenderer Renderer;

  public virtual void Init(Vector2Int tilePosition)
  {
    SetTilePosition(tilePosition);
  }

  public void SetTilePosition(Vector2Int tilePosition)
  {
    TilePosition = tilePosition;
    FixTilePosition();
  }

  public void FixTilePosition()
  {
    transform.localPosition = new Vector3(TilePosition.x, TilePosition.y, 0.0f);
  }

}
