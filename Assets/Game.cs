using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public enum Direction
{
  None,
  Left,
  Right,
  Up,
  Down,
}

public struct DirectionData
{
  public Vector2Int Offset;

  public DirectionData(Vector2Int offset)
  {
    Offset = offset;
  }
}

public class Game : MonoBehaviour
{
  public int Radius = 4;
  public GameObject TilePrefab;
  public GameObject PlayerPrefab;
  public float ShiftDuration = 1.0f;
  public Transform TilesParent;
  public float Penumbra = 2.0f;

  private int Size;
  private Tile[,] Tiles;
  private Player Player;
  private InputActions InputActions;
  public Vector2Int PlayerPosition = new Vector2Int(0, 0);
  public float NoiseScale = 5.0f;

  private DirectionData[] DirecitonDatas = new DirectionData[]
  {
    new DirectionData(),
    new DirectionData(new Vector2Int(-1, 0)),
    new DirectionData(new Vector2Int(1, 0)),
    new DirectionData(new Vector2Int(0, 1)),
    new DirectionData(new Vector2Int(0, -1)),
  };

  private void Awake()
  {
    InputActions = new InputActions();

    InputActions.Gameplay.Enable();
    InputActions.Gameplay.MoveRight.performed += MoveRight;
    InputActions.Gameplay.MoveForward.performed += MoveForward;

    Size = Radius * 2 + 3;
    Tiles = new Tile[Size, Size];

    float radiusModified = Radius + 0.5f;
    float radiusSquared = radiusModified * radiusModified;

    for (int y = 0; y < Size; y++)
    {
      for (int x = 0; x < Size; x++)
      {
        Vector2Int tilePosition = new Vector2Int(x - Radius - 1, y - Radius - 1);
        float distanceSquared = tilePosition.sqrMagnitude;
        bool isActive = true;

        if (distanceSquared > radiusSquared)
        {
          isActive = false;
        }

        GameObject tileGO = Instantiate(TilePrefab, TilesParent);
        Tile tile = tileGO.GetComponent<Tile>();
        tile.Init(tilePosition, isActive);
        tile.SetColor(GetNoiseColor(x, y));

        Tiles[x, y] = tile;
      }
    }

    GameObject playerGO = Instantiate(PlayerPrefab, transform);
    Player = playerGO.GetComponent<Player>();
    Player.Init(new Vector2Int(0, 0));

    UpdateGridShadow();
  }

  private Color GetRandomColor(float min = 0.0f)
  {
    return new Color(UnityEngine.Random.Range(min, 1.0f), UnityEngine.Random.Range(min, 1.0f), UnityEngine.Random.Range(min, 1.0f));
  }

  private Color GetNoiseColor(int x, int y)
  {
    float noiseX = (x + PlayerPosition.x) * NoiseScale;
    float noiseY = (y + PlayerPosition.y) * NoiseScale;

    float r = Mathf.PerlinNoise(noiseX, noiseY);
    r *= Mathf.PerlinNoise(noiseX * 2, noiseY * 2);
    return new Color(r, r, r);
  }

  private void MoveRight(CallbackContext callbackContext)
  {
    int value = (int)callbackContext.ReadValue<float>();
    PlayerPosition.x += value;
    
    if (value == 1) ShiftGrid(Direction.Right);
    if (value == -1) ShiftGrid(Direction.Left);
  }

  private void MoveForward(CallbackContext callbackContext)
  {
    int value = (int)callbackContext.ReadValue<float>();
    PlayerPosition.y += value;

    if (value == 1) ShiftGrid(Direction.Up);
    if (value == -1) ShiftGrid(Direction.Down);
  }

  private void ShiftGrid(Direction direction)
  {
    StartCoroutine(ShiftGridCor(direction));
  }

  private IEnumerator ShiftGridCor(Direction direction)
  {
    if(direction == Direction.None) yield break;

    InputActions.Gameplay.Disable();

    float alpha = 0.0f;
    float timeScale = 1.0f / ShiftDuration;
    Vector2 shift;
    DirectionData data = DirecitonDatas[(int)direction];

    while (alpha < 1.0f)
    {
      shift = Vector2.Lerp(new Vector2(0, 0), -data.Offset, alpha);

      TilesParent.position = shift;
      UpdateGridShadow();

      alpha += Time.deltaTime * timeScale;
      yield return null;
    }

    TilesParent.position = new Vector2(0, 0);

    Func<int, int> flipX = n => { return n; };
    Func<int, int> flipY = n => { return n; };

    if (data.Offset.x < 0)
    {
      flipX = n => { return Size - n - 1; };
    }

    if (data.Offset.y < 0)
    {
      flipY = n => { return Size - n - 1; };
    }

    for (int y = 0; y < Size; y++)
    {
      int realY = flipY(y);

      for (int x = 0; x < Size; x++)
      {
        int realX = flipX(x);

        if (IsWithin(realX + data.Offset.x, realY + data.Offset.y))
        {
          Tiles[realX + data.Offset.x, realY + data.Offset.y].CopyTo(Tiles[realX, realY]);
        }
        else
        {
          Tiles[realX, realY].SetColor(GetNoiseColor(realX, realY));
        }
      }
    }


    //UpgradeGridColors();

    UpdateGridShadow();

    InputActions.Gameplay.Enable();
  }

  private bool IsWithin(int x, int y)
  {
    if (x < 0 || x >= Size || y < 0 || y >= Size) return false;
    return true;
  }

  private void UpdateGridShadow()
  {
    float radiusModified = Radius + 0.5f;

    for (int y = 0; y < Size; y++)
    {
      for (int x = 0; x < Size; x++)
      {
        float distance = Tiles[x, y].transform.position.magnitude;

        float lightLevel = Mathf.Clamp01((radiusModified - distance) / Penumbra);

        Tiles[x, y].SetLightLevel(lightLevel);
      }
    }
  }

  private void UpgradeGridColors()
  {
    for (int y = 0; y < Size; y++)
    {
      for (int x = 0; x < Size; x++)
      {
        if (Tiles[x, y].IsActive)
        {
          Tiles[x, y].SetColor(GetNoiseColor(x, y));
        }
      }
    }
  }

  //private void OnValidate()
  //{
  //  if (Application.isPlaying)
  //  {
  //    UpdateGridShadow();
  //    UpgradeGridColors();
  //  }
  //}

}
